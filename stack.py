class Node(object):
    def __init__(self, data):
        self.data = data
        self.next = None

class Stack(object):
    def __init__(self):
        self.head = None

    def insert(self, data):
        node = Node(data)
        old_head = self.head
        self.head = node
        self.head.next = old_head

    def pop(self):
        to_return = self.head
        self.head = self.head.next
        return to_return.data


s = Stack()
s.insert(1)
s.insert(2)
print(s.pop())

