class Node(object):
    def __init__(self, data):
        self.data = data
        self.next = None


class Queue(object):
    def __init__(self):
        self.head = None
        self.tail = None

    def is_empty(self):
        return True if self.head else False

    def insert(self, data):
        node = Node(data)
        if not self.head:
            self.head = node
            self.tail = node
        else:
            self.tail.next = node
            self.tail = node

    def pop(self):
        if not self.head:
            return
        node = self.head
        self.head = self.head.next
        return node.data


q = Queue()
q.insert(1)
q.insert(2)
print(q.pop())


